
Requires MySQL and Drupal 4.7.

This module combines the features of a guestbook and shoutbox,
and adds multiuser ability. It provides a shoutbox block that
is user specific when activated for a blog page. On non blog
pages the block is user inspecific, containing all entries not
destined for any user, and suitable in a sitewide context. 

The guestbook functionality is similar giving each user a
guestbook at /shoutbook/$uid. The /shoutbook/ path without uid
returns a user inspecific guestbook - suitable in a sitewide
context. Shout entries and guestbook entries are the same. A
more link on the shoutbox block links to the guestbook for that user.

Examples:

shoutbook/3 will show all entries that were input by guests either
on user 3's blog page, or user 3's guestbook

shoutbook shows all entries not destined for a specific user

a shoutbox shown on /blog/3 will show user 3s shout entries.
Shouts input from this block will be destined for user 3

Settings:  you can choose the number of entries that appear in each block.

Permissions: 'view shoutbook': allows a role to view the guestbook and shout box block.  
             'add to shoutbook': allows a role to add entries

TODO: expose settings to each user in thier account/edit page.
If set use the user settings. If not set use the default 


Suggestions are welcome. I am a member of a discussion list for discussion
of this module and related personal blogging features.

    http://softwaremanagers.org/drupal-personal-blogs


Project page: http://drupal.org/project/shoutbook


Module Author:
David Hill a.k.a. Tatonca  <tatonca_@hotmail.com>